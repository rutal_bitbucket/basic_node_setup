"use strict";
const User = require("../models/tbluser");
var common = require("../common");

var GetAllUsers = function (objParams, callback) {
    User.find().then(function (resUsers) {
        if (resUsers.length > 0) {
            return callback({
                success: true,
                message: "Records Found !!",
                data: resUsers
            })
        }
        else {
            return callback({
                success: false,
                message: "No Records Found !!"
            })
        }
    })
}

var GetUsersById = function (objParams, callback) {
    User.findOne({
        id: objParams.id
    }).then(function (resUsers) {
        if (resUsers.length > 0) {
            return callback({
                success: true,
                message: "Records Found !!",
                data: resUsers
            })
        }
        else {
            return callback({
                success: false,
                message: "No Records Found !!"
            })
        }
    })
}

var SaveUser = function (objUser, callback) {
    if (objUser.id && objUser.id != null && objUser.id != undefined && objUser.id != 0) {
        User.findOne({
            id: objUser.id,
        }).then(function (objUserExist) {
            if (objUserExist != null) {
                if (objUser.dob != null && objUser.dob != '' && objUser.dob != undefined) {
                    objUser.dob = common.common.CurrentTime(objUser.dob);
                }
                else {
                    objUser.dob = '';
                }
                User.updateOne({
                    id: objUser.id,
                },
                    objUser
                ).then(function (response) {
                    return callback({
                        success: true,
                        message: "Profile Updated Successfully !!",
                        userid: objUser.id,
                    });
                });
            } else {
                return callback({
                    success: false,
                    message: "User Id doesn't exist",
                });
            }
        })
            .catch(function (err) {
                console.error(
                    "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                    err.message
                );
                return callback({
                    success: false,
                    message: err.message,
                });
            });
    }
    else {
        objUser.phone = "91" + objUser.phone;
        if (objUser.password != null && objUser.password != undefined && objUser.password != "") {
            objUser.password = objUser.password.trim();
        }
        if (objUser.email != null && objUser.email != undefined && objUser.email != "") {
            objUser.email = objUser.email.trim();
        }

        objUser.encryptPassword = jwt.encode(objUser.password, TokenKey);
        objUser.password = objUser.encryptPassword;
        objUser.createddate = common.common.CurrentTime();
        if (objUser.dob != null && objUser.dob != '' && objUser.dob != undefined) {
            objUser.dob = common.common.CurrentTime(objUser.dob);
        }
        else {
            objUser.dob = '';
        }

        common.GenerateUserId(function (resusername) {
            objUser.username = resusername;
            User.create(objUser).then(async function (resCreate) {
                if (resCreate) {
                    return callback({
                        success: true,
                        message: "User Created Successfully !!",
                        data: resCreate,
                    });
                }
                else {
                    return callback({
                        success: true,
                        message: "Cannot Create User !!",
                    });
                }
            });
        });
    }
};


module.exports = {
    GetAllUsers: GetAllUsers,
    GetUsersById: GetUsersById,
    SaveUser: SaveUser
}