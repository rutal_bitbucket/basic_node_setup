"use strict";
var jwt = require("jwt-simple");
var TokenKey = process.env.tokenkey;
var moment = require("moment");
const User = require('../models/tbluser');

function CurrentTime(dateToSet, flg) {
    if (dateToSet != null && dateToSet != undefined) {
        switch (flg) {
            case "start":
                dateToSet.setHours(0);
                dateToSet.setMinutes(0);
                dateToSet.setSeconds(0);
                break;
            case "end":
                dateToSet.setHours(23);
                dateToSet.setMinutes(59);
                dateToSet.setSeconds(59);
                break;
        }
        return moment(dateToSet).utc().format("x");
    }
    return moment().utc().format("x");
}

var GetNameFromDate = function (callback) {
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();

    var seconds = d.getSeconds();
    var minutes = d.getMinutes();
    var hour = d.getHours();

    var milisec = d.getMilliseconds();
    return callback(
        curr_year.toString() +
        curr_month.toString() +
        curr_date.toString() +
        hour.toString() +
        minutes.toString() +
        seconds.toString() +
        milisec.toString()
    );
};

var GenerateOTP = function () {
    return Math.floor(100000 + Math.random() * 900000);
};

var GenerateUserId = function (callback) {
    var username = Math.floor(10000000 + Math.random() * 90000000).toString();
    User.findOne({
        username: username,
    }).then(function (objUserExist) {
        if (objUserExist != null) {
            return GenerateUserId(function (ress) { });
        } else {
            return callback(username);
        }
    });
};


module.exports = {
    CurrentTime: CurrentTime,
    GetNameFromDate: GetNameFromDate,
    GenerateOTP: GenerateOTP,
    GenerateUserId: GenerateUserId
}