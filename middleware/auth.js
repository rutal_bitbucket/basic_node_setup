"use strict";
var jwt = require('jsonwebtoken');
var TokenKey = process.env.tokenkey;
var moment = require("moment");
var u = require("underscore");


var verifyToken = function (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        jwt.verify(req.token, TokenKey, (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {    
                next();
            }
        });
    } else {
        res.sendStatus(403);
    }
};

module.exports = {
    verifyToken: verifyToken
}