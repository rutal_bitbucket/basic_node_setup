var express = require('express');
var router = express.Router();
var moment = require('moment');
var common = require('../common/common');
var user = require('../common/user');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
const User = require('../models/tbluser');


router.post('/GetAllUsers', jsonParser, function (req, res) {
    user.GetAllUsers(function (resAllUsers) {
        res.json(resAllUsers);
    });
});

router.post('/GetUserById', jsonParser, function (req, res) {
    var objParams = req.body
    user.GetUsersById(objParams, function (resAllUsers) {
        res.json(resAllUsers);
    });
});


router.post('/UpdateProfile', jsonParser, function (req, res) {
    var objUpdateUser = req.body;

    user.SaveUser(objUpdateUser, null, function (response) {
        res.json(response);
    });
});


module.exports = router;