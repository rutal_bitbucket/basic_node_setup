var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var jwt = require('jsonwebtoken');
var Auth = require('../middleware/auth');
var TokenKey = process.env.tokenkey;

router.post("/test", Auth.verifyToken, jsonParser, function (req, res) {
    jwt.verify(req.token, TokenKey, (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {    
            console.log(authData);
        }
    });
    res.json({
        message: 'Post created...'
    });
});

router.post('/login', (req, res) => {
    // Mock user
    const user = {
        id: 1,
        username: 'brad',
        email: 'brad@gmail.com'
    }

    jwt.sign({ user }, TokenKey, { expiresIn: '30s' }, (err, token) => {
        res.json({
            token
        });
    });
});

module.exports = router;