var express = require('express');
var router = express.Router();
var moment = require('moment');
var common = require('../common/common');
var user = require('../common/user');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
const User = require('../models/tbluser');


router.post("/login", jsonParser, function (req, res) {
    var objParam = req.body;
    if (objParam.username == null || objParam.username == undefined || objParam.username == "" ||
        objParam.password == null || objParam.password == "" || objParam.password == undefined) {
        res.json({
            success: false,
            message: "Username or password can't be null",
        });
    }
    else {
        objParam.password = objParam.password.trim();
        var Encryptpassword = jwt.encode(objParam.password, TokenKey);
        User.aggregate([{
            $match: {
                $and: [{
                    $or: [{
                        username: objParam.username.trim()
                    },
                    {
                        phone: "91" + objParam.username
                    },
                    ],
                    password: Encryptpassword,
                },],
            },
        },
        ])
            .then(function (response) {
                if (response != null && response.length > 0) {
                    if (response[0].password == objParam.password) {
                        User.updateOne({
                            id: response[0].id,
                        }, {
                            password: Encryptpassword,
                        }, {
                            upsert: true,
                        }).then(function (respUpdate) {
                            CallFurther();
                        });
                    } else {
                        CallFurther();
                    }

                    function CallFurther() {
                        var user = {
                            username: response[0].username,
                            password: Encryptpassword,
                            Role: response[0].tblrole[0].rolename,
                            RoleType: response[0].tblrole[0].type,
                        };
                        var token = jwt.encode(user, TokenKey);
                        res.json({
                            success: true,
                            token: "JWT " + token,
                            UserId: response[0].id,
                            UserName: response[0].username,
                            UserImage: response[0].image,
                            UserVerified: response[0].isverified,
                            UserDocVerify: 1,
                            UserImage: response[0].image,
                            UserRole: user.Role,
                            UserRoleType: user.RoleType,
                            message: "Login Successfully !!",
                        });
                    }
                } else {
                    res.json({
                        success: false,
                        message: "Invalid Username or Password !!",
                    });
                }
            })
            .catch(function (err) {
                res.json({
                    success: false,
                    message: err.message,
                });
            });
    }
});

router.post("/changepassword", jsonParser, function (req, res) {
    objUser = req.body;
    account.ChangePassword(objUser, function (changePass) {
        res.json(changePass);
    });
});

router.post("/ResetPassword", jsonParser, function (req, res) {
    objUser = req.body;
    account.ResetPassword(objUser, function (respas) {
        res.json(respas);
    });
});


module.exports = router;