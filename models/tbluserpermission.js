/* jshint indent: 2 */

// module.exports = function (sequelize, DataTypes) {
//   return sequelize.define('tbluserpermission', {
//     id: {
//       type: DataTypes.INTEGER(11),
//       allowNull: false,
//       primaryKey: true,
//       autoIncrement: true
//     },
//     idmodule: {
//       type: DataTypes.INTEGER(11),
//       allowNull: true
//     },
//     idrole: {
//       type: DataTypes.INTEGER(11),
//       allowNull: true
//     },
//     added: {
//       type: DataTypes.INTEGER(1),
//       allowNull: true,
//       defaultValue: '0'
//     },
//     modified: {
//       type: DataTypes.INTEGER(1),
//       allowNull: true,
//       defaultValue: '0'
//     },
//     deleted: {
//       type: DataTypes.INTEGER(1),
//       allowNull: true,
//       defaultValue: '0'
//     },
//     show: {
//       type: DataTypes.INTEGER(1),
//       allowNull: true,
//       defaultValue: '0'
//     }
//   }, {
//     tableName: 'tbluserpermission'
//   });
// };


const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');

const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tbluserpermissionSchema = new Schema({
  id: {
    type: Number,
    // enables us to search the record faster
    index: true,
    unique: true,
    required: true
  },
  idmodule: {
    type: Number,
  },
  idrole: {
    type: Number,
  },
  added: {
    type: Number,
    default: 0
  },
  modified: {
    type: Number,
    default: 0
  },
  deleted: {
    type: Number,
    default: 0
  },
  show: {
    type: Number,
    default: 0
  }
}, schemaOptions)

tbluserpermissionSchema.plugin(autoIncrement.plugin, { model: 'tbluserpermission', field: 'id', startAt: 1, incrementBy: 1 });
var tbluserpermission = mongoose.model('tbluserpermission', tbluserpermissionSchema);
module.exports = tbluserpermission;