const mongoose = require("mongoose"),
  Schema = mongoose.Schema;
const autoIncrement = require("mongoose-plugin-autoinc");

const schemaOptions = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "last_updated",
  },
  versionKey: false,
};

let tbluserSchema = new Schema(
  {
    id: {
      type: Number,
      // enables us to search the record faster
      index: true,
      unique: true,
      required: true,
    },
    username: {
      type: String,
      default: "",
    },
    password: {
      type: String,
      default: "",
    },
    fullname: {
      type: String,
      default: "",
    },
    email: {
      type: String,
      default: "",
    },
    phone: {
      type: String,
      default: "",
    },
    dob: {
      type: String,
      default: "",
    },
    gender: {
      type: String,
      default: "",
    },
    image: {
      type: String,
      default: "",
    },
    issuspended: {
      type: Number,
      default: 0,
    },
    createddate: {
      type: String,
      default: "",
    },
  },
  schemaOptions
);

tbluserSchema.plugin(autoIncrement.plugin, {
  model: "tbluser",
  field: "id",
  startAt: 1,
  incrementBy: 1,
});
var tbluser = mongoose.model("tbluser", tbluserSchema);
module.exports = tbluser;
