/* jshint indent: 2 */

// module.exports = function (sequelize, DataTypes) {
//   return sequelize.define('tblmodule', {
//     id: {
//       type: DataTypes.INTEGER(11),
//       allowNull: false,
//       primaryKey: true,
//       autoIncrement: true
//     },
//     module: {
//       type: DataTypes.STRING(200),
//       allowNull: true
//     },
//     isactive: {
//       type: DataTypes.INTEGER(1),
//       allowNull: true,
//       defaultValue: '1'
//     },
//     displayorder: {
//       type: DataTypes.INTEGER(11),
//       allowNull: true
//     }
//   }, {
//     tableName: 'tblmodule'
//   });
// };

'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblmoduleSchema = new Schema({
  id: {
    type: Number,
    // enables us to search the record faster
    index: true,
    unique: true,
    required: true
  },
  Module: {
    type: String,
    default: ''
  },
  IsActive: {
    type: Number,
    default: 0
  },
  DisplayOrder: {
    type: Number,
    default: 0
  },
  PanelType: {
    type: String,
    default: ''
  },
  ModuleType: {
    type: Number,
  },
}, schemaOptions)

tblmoduleSchema.plugin(autoIncrement.plugin, { model: 'tblmodule', field: 'id', startAt: 1, incrementBy: 1 });
var tblmodule = mongoose.model('tblmodule', tblmoduleSchema);
module.exports = tblmodule;