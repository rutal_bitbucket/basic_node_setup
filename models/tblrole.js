/* jshint indent: 2 */

// module.exports = function (sequelize, DataTypes) {
//   return sequelize.define('tblrole', {
//     id: {
//       type: DataTypes.INTEGER(11),
//       allowNull: false,
//       primaryKey: true,
//       autoIncrement: true
//     },
//     rolename: {
//       type: DataTypes.STRING(45),
//       allowNull: false
//     },
//     description: {
//       type: DataTypes.STRING(200),
//       allowNull: true
//     },
//     level: {
//       type: DataTypes.INTEGER(11),
//       allowNull: true
//     },
//     type: {
//       type: DataTypes.STRING(45),
//       allowNull: true
//     },
//     rangemin: {
//       type: DataTypes.DECIMAL,
//       allowNull: true
//     },
//     rangemax: {
//       type: DataTypes.DECIMAL,
//       allowNull: true
//     }
//   }, {
//     tableName: 'tblrole'
//   });
// };

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblroleSchema = new Schema({
  id: {
    type: Number,
    // enables us to search the record faster
    index: true,
    unique: true,
    required: true
  },
  rolename: {
    type: String,
    default: ''
  },
  description: {
    type: String,
    default: ''
  },
  type: {
    type: String,
    default: ''
  },
  level: {
    type: Number
  },
  rangemin: {
    type: Number,
    default: 0
  },
  rangemax: {
    type: Number,
    default: 0
  },
  isdeleted: {
    type: Number,
    default: 0
  }
}, schemaOptions)

tblroleSchema.plugin(autoIncrement.plugin, { model: 'tblrole', field: 'id', startAt: 1, incrementBy: 1 });
var tblrole = mongoose.model('tblrole', tblroleSchema);
module.exports = tblrole;