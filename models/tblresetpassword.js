const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');

const schemaOptions = {
  versionKey: false
};

let tblresetpasswordSchema = new Schema({
  id: {
    type: Number,
    // enables us to search the record faster
    index: true,
    unique: true,
    required: true
  },
  userid: {
    type: Number
  },
  phone: {
    type: Number
  },
  otp: {
    type: String,
    default: ""
  },
  date: {
    type: String,
    default: ""
  },
  expirydate: {
    type: String,
    default: ""
  }
}, schemaOptions)

tblresetpasswordSchema.plugin(autoIncrement.plugin, {
  model: 'tblresetpassword',
  field: 'id',
  startAt: 1,
  incrementBy: 1
});
var tblresetpassword = mongoose.model('tblresetpassword', tblresetpasswordSchema);
module.exports = tblresetpassword;